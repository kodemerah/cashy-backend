package km.cashy.domain;

import javax.persistence.*;

@Entity
public class Vendor {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String email;

    @OneToOne
    @JoinColumn(name = "address_primary_id")
    private Address addressPrimary;

    @OneToOne
    @JoinColumn(name = "address_secondary_id")
    private Address addressSecondary;

    @Column(nullable = true)
    private String phone;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddressPrimary() {
        return addressPrimary;
    }

    public void setAddressPrimary(Address addressPrimary) {
        this.addressPrimary = addressPrimary;
    }

    public Address getAddressSecondary() {
        return addressSecondary;
    }

    public void setAddressSecondary(Address addressSecondary) {
        this.addressSecondary = addressSecondary;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
