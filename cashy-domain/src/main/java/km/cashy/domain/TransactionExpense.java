package km.cashy.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class TransactionExpense {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private double amount;

    @Column(nullable = false)
    private Date dateExpense;

    @OneToOne
    @JoinColumn(name = "transaction_id", nullable = false)
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "account_from", nullable = false)
    private Account from;

    @ManyToOne
    @JoinColumn(name = "account_category", nullable = false)
    private Account category;

    public long getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDateExpense() {
        return dateExpense;
    }

    public void setDateExpense(Date dateExpense) {
        this.dateExpense = dateExpense;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getCategory() {
        return category;
    }

    public void setCategory(Account category) {
        this.category = category;
    }
}
