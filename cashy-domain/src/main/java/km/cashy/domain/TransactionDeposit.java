package km.cashy.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class TransactionDeposit {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private double amount;

    @Column(nullable = false)
    private Date dateDeposit;

    @OneToOne
    @JoinColumn(name = "transaction_id", nullable = false)
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "account_to", nullable = false)
    private Account to;

    @ManyToOne
    @JoinColumn(name = "account_category", nullable = false)
    private Account category;

    public long getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDateDeposit() {
        return dateDeposit;
    }

    public void setDateDeposit(Date dateDeposit) {
        this.dateDeposit = dateDeposit;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public Account getCategory() {
        return category;
    }

    public void setCategory(Account category) {
        this.category = category;
    }
}
