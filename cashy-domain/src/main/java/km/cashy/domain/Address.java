package km.cashy.domain;

import javax.persistence.*;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private long id;


    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String postalCode;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    public long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
