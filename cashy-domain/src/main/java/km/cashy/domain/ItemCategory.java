package km.cashy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class ItemCategory {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = false)
    private ItemCategory parent;

    @JsonIgnore
    @OneToMany(mappedBy = "parent")
    private List<ItemCategory> childItemCategoryList;

    @JsonIgnore
    @OneToMany(mappedBy = "itemCategory")
    private List<Item> itemList;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ItemCategory getParent() {
        return parent;
    }

    public void setParent(ItemCategory parent) {
        this.parent = parent;
    }

    public List<ItemCategory> getChildItemCategoryList() {
        return childItemCategoryList;
    }

    public void setChildItemCategoryList(List<ItemCategory> childItemCategoryList) {
        this.childItemCategoryList = childItemCategoryList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
