package km.cashy.domain;

public enum CompanyType {
    FASHION,
    CAFE,
    COFFEE_SHOP
}
