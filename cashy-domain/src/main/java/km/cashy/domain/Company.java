package km.cashy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Company {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "km_user_id", nullable = false)
    private KmUser kmUser;

    @Column(nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(name = "address_primary_id")
    private Address addressPrimary;

    @OneToOne
    @JoinColumn(name = "address_secondary_id")
    private Address addressSecondary;

    @Column(nullable = false)
    private String phone;

    @Column
    private String website;

    @Column(nullable = false)
    private CompanyType type;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<Account> accountList;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<Customer> customerList;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<Vendor> vendorList;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<Item> itemList;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<ItemCategory> itemCategoryList;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<Transaction> transactionList;

    public long getId() {
        return id;
    }

    public KmUser getKmUser() {
        return kmUser;
    }

    public void setKmUser(KmUser kmUser) {
        this.kmUser = kmUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddressPrimary() {
        return addressPrimary;
    }

    public void setAddressPrimary(Address addressPrimary) {
        this.addressPrimary = addressPrimary;
    }

    public Address getAddressSecondary() {
        return addressSecondary;
    }

    public void setAddressSecondary(Address addressSecondary) {
        this.addressSecondary = addressSecondary;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public CompanyType getType() {
        return type;
    }

    public void setType(CompanyType type) {
        this.type = type;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public List<ItemCategory> getItemCategoryList() {
        return itemCategoryList;
    }

    public void setItemCategoryList(List<ItemCategory> itemCategoryList) {
        this.itemCategoryList = itemCategoryList;
    }

    public List<Vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<Vendor> vendorList) {
        this.vendorList = vendorList;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
