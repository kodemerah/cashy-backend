package km.cashy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Account {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String code;

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    private Account parent;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @Column(nullable = false)
    private double balance;

    @Column(nullable = false)
    private boolean isPrimary;

    @JsonIgnore
    @OneToMany(mappedBy = "parent")
    private List<Account> childAccountList;

    @JsonIgnore
    @OneToMany(mappedBy = "to")
    private List<TransactionDeposit> depositToList;

    @JsonIgnore
    @OneToMany(mappedBy = "category")
    private List<TransactionDeposit> depositCategoryList;

    @JsonIgnore
    @OneToMany(mappedBy = "from")
    private List<TransactionExpense> expenseFromList;

    @JsonIgnore
    @OneToMany(mappedBy = "category")
    private List<TransactionExpense> expenseCategoryList;

    @JsonIgnore
    @OneToMany(mappedBy = "from")
    private List<TransactionTransfer> transferFromList;

    @JsonIgnore
    @OneToMany(mappedBy = "to")
    private List<TransactionTransfer> transferToList;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Account getParent() {
        return parent;
    }

    public void setParent(Account parent) {
        this.parent = parent;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public List<Account> getChildAccountList() {
        return childAccountList;
    }

    public void setChildAccountList(List<Account> childAccountList) {
        this.childAccountList = childAccountList;
    }

    public List<TransactionDeposit> getDepositToList() {
        return depositToList;
    }

    public void setDepositToList(List<TransactionDeposit> depositToList) {
        this.depositToList = depositToList;
    }

    public List<TransactionDeposit> getDepositCategoryList() {
        return depositCategoryList;
    }

    public void setDepositCategoryList(List<TransactionDeposit> depositCategoryList) {
        this.depositCategoryList = depositCategoryList;
    }

    public List<TransactionExpense> getExpenseFromList() {
        return expenseFromList;
    }

    public void setExpenseFromList(List<TransactionExpense> expenseFromList) {
        this.expenseFromList = expenseFromList;
    }

    public List<TransactionExpense> getExpenseCategoryList() {
        return expenseCategoryList;
    }

    public void setExpenseCategoryList(List<TransactionExpense> expenseCategoryList) {
        this.expenseCategoryList = expenseCategoryList;
    }

    public List<TransactionTransfer> getTransferFromList() {
        return transferFromList;
    }

    public void setTransferFromList(List<TransactionTransfer> transferFromList) {
        this.transferFromList = transferFromList;
    }

    public List<TransactionTransfer> getTransferToList() {
        return transferToList;
    }

    public void setTransferToList(List<TransactionTransfer> transferToList) {
        this.transferToList = transferToList;
    }
}
