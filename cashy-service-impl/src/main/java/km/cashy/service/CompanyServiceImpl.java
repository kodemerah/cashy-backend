package km.cashy.service;

import km.cashy.domain.Company;
import km.cashy.domain.KmUser;
import km.cashy.v1.repository.CompanyRepository;
import km.cashy.v1.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public void createCompany(Company company) {
        companyRepository.save(company);
    }

    @Override
    public List<Company> getCompanyList() {
        return companyRepository.findAll();
    }

    @Override
    public Company updateCompany(long id, Company company, KmUser kmUser) {
        Company oldCompany = companyRepository.findOne(id);
        oldCompany.setKmUser(kmUser);
        oldCompany.setType(company.getType());
        oldCompany.setName(company.getName());
        oldCompany.setPhone(company.getPhone());
        oldCompany.setAccountList(company.getAccountList());
        oldCompany.setAddressPrimary(company.getAddressPrimary());
        oldCompany.setAddressSecondary(company.getAddressSecondary());
        oldCompany.setCustomerList(company.getCustomerList());
        oldCompany.setItemList(company.getItemList());
        oldCompany.setItemCategoryList(company.getItemCategoryList());
        oldCompany.setTransactionList(company.getTransactionList());
        oldCompany.setVendorList(company.getVendorList());
        oldCompany.setWebsite(company.getWebsite());
        companyRepository.save(oldCompany);
        return oldCompany;
    }
}
