package km.cashy.service;

import km.cashy.v1.service.KmUserService;
import km.cashy.domain.KmUser;
import km.cashy.v1.repository.KmUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;

@Component
public class KmUserServiceImpl implements KmUserService {

    @Autowired
    private KmUserRepository kmUserRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public List<KmUser> getAll() {
        return kmUserRepository.findAll();
    }

    @Override
    public KmUser addNewUser(String username, String password) {
        KmUser user = new KmUser();
        user.setUsername(username);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        kmUserRepository.save(user);
        return user;
    }

    @Override
    public KmUser loadKmUserByUsername(String username) {
        return kmUserRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        KmUser kmUser = loadKmUserByUsername(s);
        return new User(kmUser.getUsername(), kmUser.getPassword(),
                emptyList());
    }
}
