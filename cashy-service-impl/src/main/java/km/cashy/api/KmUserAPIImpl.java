package km.cashy.api;

import km.cashy.v1.api.KmUserAPI;
import km.cashy.v1.response.GetUserDetailResponse;
import km.cashy.v1.service.KmUserService;
import km.cashy.domain.KmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/user")
@ComponentScan(basePackages = {"km.cashy.api", "km.cashy.v1.service", "km.cashy.service"})
public class KmUserAPIImpl implements KmUserAPI {

    @Autowired
    private KmUserService kmUserService;

    @GetMapping(path="/add")
    public @ResponseBody
    KmUser addNewUser (@RequestParam String username, @RequestParam String password) {
        KmUser newUser = kmUserService.addNewUser(username, "password");
        return newUser;
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<KmUser> getAllUsers() {
        return kmUserService.getAll();
    }

    @PostMapping(path = "/register")
    public @ResponseBody KmUser registration(@RequestBody KmUser kmUser){
        KmUser newUser = kmUserService.addNewUser(
                kmUser.getUsername(),
                kmUser.getPassword());
        return newUser;
    }

    @GetMapping(path = "/detail")
    public @ResponseBody
    GetUserDetailResponse getUserDetails() {
        GetUserDetailResponse response = new GetUserDetailResponse();
        KmUser user = (KmUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        response.setKmUser(user);
        response.setCompanyList(user.getCompanyList());
        return response;
    }
}