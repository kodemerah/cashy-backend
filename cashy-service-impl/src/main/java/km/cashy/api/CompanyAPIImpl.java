package km.cashy.api;

import km.cashy.domain.Company;
import km.cashy.domain.KmUser;
import km.cashy.v1.api.CompanyAPI;
import km.cashy.v1.request.CreateCompanyRequest;
import km.cashy.v1.request.EditCompanyRequest;
import km.cashy.v1.response.CreateCompanyResponse;
import km.cashy.v1.response.EditCompanyResponse;
import km.cashy.v1.response.GetCompanyListResponse;
import km.cashy.v1.response.HttpApiResponse;
import km.cashy.v1.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/company")
@ComponentScan(basePackages = {"km.cashy.api", "km.cashy.v1.service", "km.cashy.service"})
public class CompanyAPIImpl implements CompanyAPI {

    @Autowired
    CompanyService companyService;

    private final HttpApiResponse httpApiResponse = new HttpApiResponse();

    @PostMapping(path = "/create")
    public ResponseEntity<CreateCompanyResponse> createNewCompany(@RequestBody CreateCompanyRequest request) {
        KmUser USER_AUTHENTICATED = (KmUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Company company = new Company();
        company.setName(request.getName());
        company.setPhone(request.getPhone());
        company.setType(request.getType());
        company.setAddressPrimary(request.getAddressPrimary());
        company.setAddressSecondary(request.getAddressSecondary());
        company.setKmUser(USER_AUTHENTICATED);
        company.setWebsite(request.getWebsite());
        companyService.createCompany(company);
        CreateCompanyResponse response = new CreateCompanyResponse();
        response.setCompany(company);
        return httpApiResponse.createdResponse(response);
    }

    @PostMapping(path = "/update")
    public ResponseEntity<EditCompanyResponse> editCompany(@RequestBody EditCompanyRequest request) {
        KmUser USER_AUTHENTICATED = (KmUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        EditCompanyResponse response = new EditCompanyResponse();
        response.setCompany(companyService.updateCompany(request.getId(), request.getCompany(), USER_AUTHENTICATED));
        return httpApiResponse.successResponse(response);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<GetCompanyListResponse> getCompanyList() {
        GetCompanyListResponse response = new GetCompanyListResponse();
        response.setCompanyList(companyService.getCompanyList());
        return httpApiResponse.successResponse(response);
    }
}
