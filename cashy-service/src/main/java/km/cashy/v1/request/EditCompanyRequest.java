package km.cashy.v1.request;

import km.cashy.domain.Company;

public class EditCompanyRequest {
    private long id;
    private Company company;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
