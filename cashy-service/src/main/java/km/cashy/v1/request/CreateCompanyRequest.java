package km.cashy.v1.request;

import km.cashy.domain.Address;
import km.cashy.domain.CompanyType;

public class CreateCompanyRequest {
    private String name;
    private String phone;
    private CompanyType type;
    private Address addressPrimary;
    private Address addressSecondary;
    private String website;

    public String getWebsite() {
        return website;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public CompanyType getType() {
        return type;
    }

    public Address getAddressPrimary() {
        return addressPrimary;
    }

    public Address getAddressSecondary() {
        return addressSecondary;
    }
}
