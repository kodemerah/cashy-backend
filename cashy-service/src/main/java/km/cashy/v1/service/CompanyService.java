package km.cashy.v1.service;

import km.cashy.domain.Company;
import km.cashy.domain.KmUser;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CompanyService {
    void createCompany(Company company);
    List<Company> getCompanyList();
    Company updateCompany(long id, Company company, KmUser user);
}
