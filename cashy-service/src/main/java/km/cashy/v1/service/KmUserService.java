package km.cashy.v1.service;

import km.cashy.domain.KmUser;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KmUserService extends UserDetailsService{
    List<KmUser> getAll();
    KmUser addNewUser(String username, String password);
    KmUser loadKmUserByUsername(String username);
}

