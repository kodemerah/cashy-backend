package km.cashy.v1.repository;

import km.cashy.domain.TransactionDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionDepositRepository extends JpaRepository<TransactionDeposit, Long> {
}
