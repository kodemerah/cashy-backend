package km.cashy.v1.repository;

import km.cashy.domain.KmUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface KmUserRepository extends JpaRepository<KmUser, Long> {

    @Query("SELECT kmuser FROM KmUser kmuser WHERE kmuser.username = :username")
    KmUser findByUsername(@Param("username") String username);
}