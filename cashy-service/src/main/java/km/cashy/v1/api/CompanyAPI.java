package km.cashy.v1.api;

import km.cashy.domain.Company;
import km.cashy.v1.request.CreateCompanyRequest;
import km.cashy.v1.request.EditCompanyRequest;
import km.cashy.v1.response.CreateCompanyResponse;
import km.cashy.v1.response.EditCompanyResponse;
import km.cashy.v1.response.GetCompanyListResponse;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

public interface CompanyAPI {
    ResponseEntity<CreateCompanyResponse> createNewCompany (CreateCompanyRequest request);

    ResponseEntity<EditCompanyResponse> editCompany(EditCompanyRequest request);

    ResponseEntity<GetCompanyListResponse> getCompanyList();
}
