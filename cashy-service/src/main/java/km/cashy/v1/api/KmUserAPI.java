package km.cashy.v1.api;

import km.cashy.domain.KmUser;
import km.cashy.v1.response.GetUserDetailResponse;
import org.springframework.web.bind.annotation.*;

public interface KmUserAPI {
    @ResponseBody KmUser addNewUser (@RequestParam String username, @RequestParam String password);

    @ResponseBody Iterable<KmUser> getAllUsers();

    @ResponseBody KmUser registration(@RequestBody KmUser kmUser);

    @ResponseBody GetUserDetailResponse getUserDetails();
}
