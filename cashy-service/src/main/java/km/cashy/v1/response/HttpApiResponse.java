package km.cashy.v1.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HttpApiResponse {

    public <T> ResponseEntity<T> successResponse(T t){
        ResponseEntity<T> responseEntity = new ResponseEntity<>(t, HttpStatus.OK);
        return responseEntity;
    }

    public <T> ResponseEntity<T> failedResponse(T t){
        ResponseEntity<T> responseEntity = new ResponseEntity<>(t, HttpStatus.NOT_FOUND);
        return responseEntity;
    }

    public <T> ResponseEntity<T> createdResponse(T t){
        ResponseEntity<T> responseEntity = new ResponseEntity<>(t, HttpStatus.CREATED);
        return responseEntity;
    }

    public <T> ResponseEntity<T> unauthorizedResponse(T t){
        ResponseEntity<T> responseEntity = new ResponseEntity<>(t, HttpStatus.UNAUTHORIZED);
        return responseEntity;
    }
}
