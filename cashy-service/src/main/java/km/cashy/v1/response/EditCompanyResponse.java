package km.cashy.v1.response;

import km.cashy.domain.Company;

public class EditCompanyResponse {
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
