package km.cashy.v1.response;

import km.cashy.domain.Company;
import km.cashy.domain.KmUser;

import java.util.List;

public class GetUserDetailResponse {
    private KmUser kmUser;
    private List<Company> companyList;

    public KmUser getKmUser() {
        return kmUser;
    }

    public List<Company> getCompanyList() {
        return companyList;
    }

    public void setKmUser(KmUser kmUser) {
        this.kmUser = kmUser;
    }

    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
    }
}
