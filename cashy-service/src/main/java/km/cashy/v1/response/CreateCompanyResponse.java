package km.cashy.v1.response;

import km.cashy.domain.Company;

public class CreateCompanyResponse {
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
