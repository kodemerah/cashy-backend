package km.cashy.v1.response;

import km.cashy.domain.Company;

public class GetCompanyListResponse {
    private Iterable<Company> companyList;

    public Iterable<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(Iterable<Company> companyList) {
        this.companyList = companyList;
    }
}
