package km.cashy.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"km.cashy.v1.repository"})
@EntityScan(basePackages = {"km.cashy.domain"})
@ComponentScan(basePackages = {"km.cashy.api", "km.cashy.v1.service", "km.cashy.service", "km.cashy.security"})
public class CashyApplication extends SpringBootServletInitializer {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(CashyApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CashyApplication.class);
        app.run(args);
    }

}

