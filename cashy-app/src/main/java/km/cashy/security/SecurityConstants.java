package km.cashy.security;

public class SecurityConstants {
    public static final String SECRET = "secret_token";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/user/register";
    public static final String API_DOCS_URL = "/v2/api-docs";
    public static final String SWAGGER_API_URL = "/swagger-ui.html";
    public static final String SWAGGER_RESOURCE_URL = "/configuration/**";
    public static final long EXPIRATION_TIME = 864_000_000L;
}
